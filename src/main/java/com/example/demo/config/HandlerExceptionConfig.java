package com.example.demo.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.http.HttpStatus;

import com.example.demo.bean.ErrorResponse;

import lombok.extern.slf4j.Slf4j;
import com.example.demo.exception.*;

@Slf4j
@RestControllerAdvice
public class HandlerExceptionConfig {

	
    @Autowired
    private MessageConfig messageConfig;

    @ExceptionHandler(value = {Exception.class})
    @ResponseStatus(HttpStatus.OK)
    public ErrorResponse exception(Exception ex) {
        log.error("Exception : ", ex);
        ErrorResponse resp = new ErrorResponse();
        resp.getHeaderResp().setService("test");
        resp.getHeaderResp().setStatusCd("9999");
        resp.getHeaderResp().setStatusDesc(ex.getMessage());
        resp.getHeaderResp().getErrorDisplay().setTitle(messageConfig.getMessageSource("error9999_title"));
        resp.getHeaderResp().getErrorDisplay().setMessage(messageConfig.getMessageSource("error9999_message"));
        return resp;
    }
    
    @ExceptionHandler(value = {BusinessException.class})
    @ResponseStatus(HttpStatus.OK)
    public ErrorResponse handleBusinessException(BusinessException ex) {
        log.error("Business Exception : ", ex);
        ErrorResponse errorResponse = new ErrorResponse();
        errorResponse.getHeaderResp().setStatusCd(ex.getCode());
        errorResponse.getHeaderResp().setStatusDesc(ex.getMessage());
        errorResponse.getHeaderResp().getErrorDisplay().setTitle(messageConfig.getMessageSource("error"+ex.getCode()+"_title"));
        errorResponse.getHeaderResp().getErrorDisplay().setMessage(messageConfig.getMessageSource("error"+ex.getCode()+"_message"));
        return errorResponse;
    }
}
