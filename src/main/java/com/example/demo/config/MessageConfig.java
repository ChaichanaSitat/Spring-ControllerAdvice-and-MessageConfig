package com.example.demo.config;

import java.text.MessageFormat;
import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageConfig {

    @Autowired
    private MessageSource messageSource;

    public String getMessageSource(String msg) {
        return messageSource.getMessage(msg, null, new Locale("th"));
    }

    public String getMessageSourceFmt(String msg, Object[] replaceValue) {

        String errorMessage = messageSource.getMessage(msg, null, new Locale("th"));
        MessageFormat fmt = new MessageFormat(errorMessage);
        return fmt.format(replaceValue);

    }

}

