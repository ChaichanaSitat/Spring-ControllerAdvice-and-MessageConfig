package com.example.demo.bean;

import lombok.*;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDisplayBean {

    private String title;
    private String message;

}
