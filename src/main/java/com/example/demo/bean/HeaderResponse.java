package com.example.demo.bean;

import com.fasterxml.jackson.annotation.JsonAlias;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class HeaderResponse implements Serializable {

    private static final long serialVersionUID = 2367978280736122674L;
    private String service;
    @JsonAlias({"statusCd", "statusCode"})
    private String statusCd = "0000";
    private String statusDesc = "SUCCESS";
    private ErrorDisplayBean errorDisplay = ErrorDisplayBean.builder().build();

}
