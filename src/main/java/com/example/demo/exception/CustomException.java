package com.example.demo.exception;

public class CustomException extends Exception {

    /**
	 * 
	 */
	private static final long serialVersionUID = 7158090368375256380L;
	private final String message;
    private String code;
    private String displayTitle;
    private String displayMessage;

    public CustomException(String message) {
        super(message);
        this.message = message;
    }

    protected CustomException(String code, String message) {
        super(message);
        this.code = code;
        this.message = message;
    }

    public CustomException(String message, String code, String displayTitle, String displayMessage) {
        super(message);
        this.message = message;
        this.code = code;
        this.displayTitle = displayTitle;
        this.displayMessage = displayMessage;
    }

    public String getMessage() {
        return message;
    }

    public String getCode() {
        return code;
    }
}

