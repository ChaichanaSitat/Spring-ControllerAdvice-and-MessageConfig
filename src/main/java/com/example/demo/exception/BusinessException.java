package com.example.demo.exception;

public class BusinessException extends CustomException {

	 public BusinessException(String message) {
	        super(message);
	    }

	    public BusinessException(String code, String message) {
	        super(code, message);
	    }

	    public BusinessException(String code, String message, String displayTitle, String displayMessage) {
	        super(code, message, displayTitle, displayMessage);
	    }

}
